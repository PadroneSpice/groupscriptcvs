# README #

Krita Python script to generate a layer group in the current opened document with the following (top to bottom):
by Sean Castillo - 29 April 2020

Colors: Set to the 'Color' blending mode, and with Alpha Inheritance toggled on
Values: Use this to paint the values
Sketch: Use this for sketching

The group itself is given the name "Colors-Values-Sketch Group".
Colors is labelled purple.
Values is labelled grey.
Sketch is labelled orange.
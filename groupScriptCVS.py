## Script to create a group consisting of:
## Colors, Values, and Sketch
## The Colors layer is set to the 'Color' blending mode
## and its alpha inheritance is on.
## Upgrade on 19 April 2021: The sketch layer is now tagged blue
## to work with the "Export No Draft" system.


from krita import *

doc  = Krita.instance().activeDocument()
if not doc is None:
    root = doc.rootNode();

    group = doc.createGroupLayer("Colors-Values-Sketch Group")
    root.addChildNode(group, None)

    layerNames = ["Sketch", "Values", "Colors"]
    layerColors = [1, 8, 7]

    # Generate the bottom two layers.
    for i in range(len(layerNames)-1):
        newLayer = doc.createNode(layerNames[i], "paintLayer")
        newLayer.setColorLabel(layerColors[i])
        group.addChildNode(newLayer, None)

    # The Colors layer has a couple more settings to set.
    colorsLayer = doc.createNode(layerNames[2], "paintLayer")
    colorsLayer.setColorLabel(layerColors[2])
    colorsLayer.setBlendingMode("color")
    colorsLayer.setInheritAlpha(True)
    group.addChildNode(colorsLayer, None)
